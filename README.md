INTERROBANG: a tiny launcher menu packing a big bang (syntax)

Interrobang is a launcher menu somewhat similar to dmenu.  Interrobang has two unique features:

1) Tab completion for both commands and filename parameters (depends on bash completion)

2) A "bang syntax" inspired by the duckduckgo search engine.

User defined bangs, and the look/feel of interrobang can be customized in ~/.interrobangrc.  A template for this file is available in this repo, but it is not installed with the Makefile or PKGBUILD - you must copy it manually, or just use it as a template for your own.

Interrobang is brand new (1 day old at the time of this edit) and under active development.  This page will be udpated periodically, but for the most up to date information, see here:
https://bbs.archlinux.org/viewtopic.php?pid=1248447#p1248447

